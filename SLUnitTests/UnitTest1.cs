﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SLBusiness;
using SLBusiness.Iso8583;

namespace SLUnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestIso8583()
        {
            var iso8583 = new ClientIso8583();

            var packet = iso8583.GenerateIso8583Packet("1234567890123456789", 888);

            Assert.AreEqual(Constants.TEL_TYPE_CLIENT, packet.Substring(0, 4), "cheking packet type");

            Assert.AreEqual("1234567890123456789", packet.Substring(22, 19), "checking PAN section");

            Assert.AreEqual("888".PadRight(12), packet.Substring(47, 12), "checking amount section");
        }
    }
}
