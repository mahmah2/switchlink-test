﻿using SLBusiness;
using System;

namespace SLTestServer
{
    class Program
    {
        private static bool shutdown = false;

        private static string PROMPT = ">";

        static void Main(string[] args)
        {
            var server = new ServerService("127.0.0.1", Constants.COMM_PORT_NO);

            server.OnShutdownCommand += Server_OnShutdownCommand;

            Console.Write(PROMPT);
            var cmd = Console.ReadLine();
            while (!shutdown) 
            {
                server.RunCommand(cmd);

                if (shutdown)
                    break;

                Console.Write(PROMPT);
                cmd = Console.ReadLine();
            }
        }

        private static void Server_OnShutdownCommand(object sender, EventArgs e)
        {
            shutdown = true;
        }
    }
}
