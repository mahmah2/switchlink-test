﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLBusiness
{
    public static class Constants
    {
        public const string CMD_SHUTDOWN = "SHUTDOWN";
        public const string CMD_CUTOVER = "CUTOVER";
        public const string CMD_RECONCILE = "RECONCILE";
        public const string CMD_REALTIME = "REALTIME";

        public const int COMM_PORT_NO = 8080;

        public const string TEL_TYPE_CLIENT = "0200";
        public const string TEL_TYPE_SERVER = "0210";

    }
}
