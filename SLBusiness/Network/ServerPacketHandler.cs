﻿using Networker.Common;
using Networker.Common.Abstractions;
using SLBusiness.Iso8583;
using System;
using System.Threading.Tasks;

namespace SLBusiness
{
    internal class ServerPacketHandler : PacketHandlerBase<string>
    {
        public static event EventHandler<string> OnProcess;

        public ServerPacketHandler()
        {
            
        }

        public override async Task Process(string packet, IPacketContext packetContext)
        {
            OnProcess.Invoke(packetContext, packet);
        }
    }
}