﻿using Networker.Common.Abstractions;
using Networker.Extensions.MessagePack;
using Networker.Server;
using Networker.Server.Abstractions;
using SLBusiness.Iso8583;
using System;

namespace SLBusiness
{
    public class ServerService
    {
        private ServerIso8583 _serverIso8583;
        private IServer _server;
        private bool _realtimeMode;

        public event EventHandler OnShutdownCommand;

        public ServerService(string ip, int portNo)
        {
            _serverIso8583 = new ServerIso8583();
            _realtimeMode = false;

            _server = new ServerBuilder().
                UseTcp(portNo).
                RegisterPacketHandler<string, ServerPacketHandler>().
                UseMessagePack().
                Build();

            ServerPacketHandler.OnProcess += ServerPacketHandler_OnProcess;

            _server.ClientConnected = clientConnected;

            _server.Start();
        }

        void clientConnected(object sender, TcpConnectionConnectedEventArgs e)
        {

        }

        private ISender _sender;

        private void ServerPacketHandler_OnProcess(object sender, string e)
        {
            var response = _serverIso8583.Digest(e);

            _sender = (sender as IPacketContext).Sender;

            (sender as IPacketContext).Sender.Send<string>($"{response}");

            if (_realtimeMode)
            {
                Console.WriteLine($">{e}");
                Console.WriteLine($"<{response}");
            }
        }

        ~ServerService()
        {

        }

        public bool RunCommand(string cmd)
        {
            if (cmd == Constants.CMD_RECONCILE)
            {
                _serverIso8583.Reconcile();
                return true;
            }
            else if (cmd== Constants.CMD_SHUTDOWN)
            {
                _serverIso8583.StartShutDown(); //will not come back until its safe to shutdown

                OnShutdownCommand?.Invoke(this, EventArgs.Empty);

                return true;
            }
            else if (cmd == Constants.CMD_CUTOVER)
            {
                _sender?.Send<string>(Constants.CMD_CUTOVER);

                return true;
            }
            else if (cmd == Constants.CMD_REALTIME)
            {
                _realtimeMode = true;
                return true;
            }

            return false;
        }

    }
}
