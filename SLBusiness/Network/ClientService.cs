﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Networker.Client;
using Networker.Client.Abstractions;
using Networker.Extensions.MessagePack;
using SLBusiness.Iso8583;

namespace SLBusiness
{
    public class ClientService
    {
        private bool _connected = false;
        private ClientIso8583 _clientIso8583 = new ClientIso8583();


        public ClientService(string ip , int portNo)
        {
            IClient client = null;

            ClientPacketHandler.OnCutoverCommand += ClientPacketHandler_OnCutoverCommand;

            Task.Run(()=> {

                while (true)
                {
                    try
                    {
                        if (_connected)
                        {
                            client.Send<string>(_clientIso8583.GenerateIso8583RandomPacket());
                        }
                        else
                        {
                            try
                            {
                                Console.WriteLine("Trying to connect...");
                                client = buildClient(ip, portNo);
                                client.Connect();
                            }
                            catch 
                            {
                                Console.WriteLine("Exception in establishing connection.");
                            }
                        }

                        Thread.Sleep(1000);
                    }
                    catch 
                    {
                    }
                }


            });

        }

        private void ClientPacketHandler_OnCutoverCommand(object sender, EventArgs e)
        {
            _clientIso8583.Cutover();
        }

        private IClient buildClient(string ip, int portNo)
        {
            IClient client = new ClientBuilder().
                                                        UseIp(ip).
                                                        UseTcp(portNo).
                                                        UseMessagePack().
                                                        RegisterPacketHandler<string, ClientPacketHandler>().
                                                        Build();
            client.Connected += OnConnect;
            client.Disconnected += OnDisconnect;

            return client;
        }

        private void OnConnect(object o, Socket s)
        {
            Console.WriteLine("Connected!");
            _connected = true;
        }

        private void OnDisconnect(object o, Socket s)
        {
            Console.WriteLine("Disconnected!");
            _connected = false;
        }

        ~ClientService()
        {

        }
    }
}
