﻿using Networker.Common;
using Networker.Common.Abstractions;
using System;
using System.Threading.Tasks;

namespace SLBusiness
{
    internal class ClientPacketHandler : PacketHandlerBase<string>
    {
        public ClientPacketHandler()
        {
            
        }

        public static event EventHandler OnCutoverCommand;

        public override async Task Process(string packet, IPacketContext packetContext)
        {
            if (packet == "CUTOVER")
            {
                OnCutoverCommand.Invoke(this, EventArgs.Empty);
            }
        }
    }
}