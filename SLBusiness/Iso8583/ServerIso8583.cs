﻿using SLBusiness.DataConversion;
using SLBusiness.Encryption;
using System;
using System.Collections.Generic;

namespace SLBusiness.Iso8583
{
    public class ServerIso8583
    {
        private Dictionary<string, int> _stats = new Dictionary<string, int>();

        private object _shutdownLock = new object();
        private bool _shuttingDown;

        public ServerIso8583()
        {
            _shuttingDown = false;
        }

        public string Digest(string rawData)
        {
            if (_shuttingDown)
                return string.Empty;

            lock (_shutdownLock)
            {
                ISO8583 iso8583 = new ISO8583();

                string[] DE;

                DE = iso8583.Parse(rawData);

                var MTI = DE[0];
                var secondaryBitmap = DE[1];
                var PAN = DE[2];
                var processingCode = DE[3];
                var transactionAmount = int.Parse(DE[4]);
                var transactionDateTime = DE[7];
                var billingFee = DE[8];
                var STAN = DE[11];
                var expiryDate = DE[14];
                var settlementDate = DE[15];
                var hash = DE[64];

                SaveStat(settlementDate, transactionAmount);

                var computedHash = DataConvert.GetBytesToString(
                                        BitConverter.GetBytes(
                                            Algorithms.To32BitFnv1aHash(
                                                rawData.Substring(0, rawData.Length - 8))));

                if (hash != computedHash)
                {
                    //Handle corrupt telegram
                }


                ISO8583 iso8583Response = new ISO8583();
                string[] responseDE = new string[130];

                responseDE[2] = PAN;
                responseDE[3] = processingCode;
                responseDE[4] = transactionAmount.ToString();
                responseDE[7] = transactionDateTime;
                responseDE[8] = billingFee;
                responseDE[11] = STAN;
                responseDE[14] = expiryDate;
                responseDE[15] = settlementDate;
                responseDE[64] = hash;

                return iso8583Response.Build(responseDE, Constants.TEL_TYPE_SERVER);
            }
        }

        internal void StartShutDown()
        {
            lock (_shutdownLock)
            {
                _shuttingDown = true;
            }
        }

        public void Reconcile()
        {
            Console.WriteLine($"Settlement Date|Amount");

            var statsSnapshot = new Dictionary<string, int>(_stats);

            foreach (var item in statsSnapshot)
            {
                Console.WriteLine($"{item.Key.PadLeft(15)}| {item.Value.ToString()}");
            }
        }

        private void SaveStat(string settlementDate, int transactionAmount)
        {
            if (_stats.ContainsKey(settlementDate))
            {
                _stats[settlementDate] += transactionAmount;
            }
            else
            {
                _stats.Add(settlementDate, transactionAmount);
            }
        }
    }
}
